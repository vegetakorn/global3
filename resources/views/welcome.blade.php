<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Global3Corp</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="css/iofrm-style.css">
    <link rel="stylesheet" type="text/css" href="css/iofrm-theme2.css">
</head>
<body style="background-color: #23394A">
<div class="form-body">
    <div class="website-logo">
        <a href="http://global3corp.com/index.html">
            <div class="logo">
                <img class="logo-size" src="images/logo.png" alt="">
            </div>
        </a>
    </div>
    <div class="row" style="height: 100%">
        <div class="img-holder">
            <div class="bg"></div>
            <div class="info-holder">

            </div>
        </div>

        <div class="form-holder" style="height: 100%">
            <div style="width: 100%; background-color: #FFF" >
                <a href="http://global3corp.com/index.html">  <img width="98%" class="logo-size" src="images/banner.png" alt=""></a>
            </div>

            <div class="container " style="height: 100%" >

                <div  class="row  justify-content-center" style="position: center;  text-align: center"   >
                    <div class="form-content " style="position: center;  text-align: center;height: 100%" >

                        <div class="form-items " style="position: center; align-content: center;align-items: center; text-align: center">
                            <h3 class="text-center">¡Déjanos tu mensaje!</h3>
                            <p class="text-center">¡Contáctanos y recibe un 20% de descuento en tu primer mensualidad!</p>

                            <form>
                                <input class="form-control" type="text" name="name" placeholder="Nombre" required>
                                <input class="form-control" type="email" name="email" placeholder="Correo electrónico" required>
                                <input class="form-control" type="text" name="name" placeholder="Teléfono" >
                                <div class="form-button">
                                    <button id="submit" type="submit" class="ibtn">Registrarse</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="row" style="position: center;  text-align: center" >
                    <div class="col-sm">
                        <div style="color:#FFF" class="text-center p-4 mt-3">
                            <div  class="avatar-md mx-auto mb-4">
                                <span class="avatar-title rounded-circle bg-soft-primary">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-grid icon-dual-primary"><rect x="3" y="3" width="7" height="7"></rect><rect x="14" y="3" width="7" height="7"></rect><rect x="14" y="14" width="7" height="7"></rect><rect x="3" y="14" width="7" height="7"></rect></svg>
                                </span>
                            </div>
                            <h5 class="font-18">ESPACIO DE OFICINA</h5>
                            <p class="mb-0">Espacios físicos para su negocio</p>
                        </div>
                    </div>
                    <div class="col-sm">
                        <div style="color:#FFF" class="text-center p-4 mt-3">
                            <div  class="avatar-md mx-auto mb-4">
                                <span class="avatar-title rounded-circle bg-soft-primary">
                                   <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-layers icon-dual-primary"><polygon points="12 2 2 7 12 12 22 7 12 2"></polygon><polyline points="2 17 12 22 22 17"></polyline><polyline points="2 12 12 17 22 12"></polyline></svg>
                                </span>
                            </div>
                            <h5 class="font-18">OFICINA VIRTUAL</h5>
                            <p class="mb-0">Concéntrece en el desarrollo de sus proyecto, mientras nosotros nos encargamos de la parte adminsitrativa.</p>
                        </div>
                    </div>
                    <div class="col-sm">
                        <div style="color:#FFF" class="text-center p-4 mt-3">
                            <div  class="avatar-md mx-auto mb-4">
                                <span class="avatar-title rounded-circle bg-soft-primary">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-tablet icon-dual-primary"><rect x="4" y="2" width="16" height="20" rx="2" ry="2"></rect><line x1="12" y1="18" x2="12.01" y2="18"></line></svg>
                                </span>
                            </div>
                            <h5 class="font-18">COWORKING</h5>
                            <p class="mb-0">Escritorios compartidos</p>
                        </div>
                    </div>
                </div>
                <div class="row" style="position: center;  text-align: center" >
                    <div class="col-sm">

                    </div>
                    <div class="col-sm">
                        <div style="color:#FFF" class="text-center p-4 mt-3">
                            <div  class="avatar-md mx-auto mb-4">
                                <span class="avatar-title rounded-circle bg-soft-primary">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-grid icon-dual-primary"><rect x="3" y="3" width="7" height="7"></rect><rect x="14" y="3" width="7" height="7"></rect><rect x="14" y="14" width="7" height="7"></rect><rect x="3" y="14" width="7" height="7"></rect></svg>
                                </span>
                            </div>
                            <h5 class="font-18">SALA DE JUNTAS</h5>
                            <p class="mb-0">Realice presentaciones y cierre sus proyectos en su sala de juntas</p>
                        </div>
                    </div>
                    <div class="col-sm">

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<script src="js/jquery.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/main.js"></script>
</body>
</html>