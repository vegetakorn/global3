<!DOCTYPE html>
<html lang="en">
<head>
    <title>Global3Corp</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="v18/images/icons/favicon.ico"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="v18/vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="v18/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="v18/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="v18/vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="v18/vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="v18/vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="v18/vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="v18/vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="v18/css/util.css">
    <link rel="stylesheet" type="text/css" href="v18/css/main.css">
    <!--===============================================================================================-->
</head>
<body style="background-color: #23394A;">
<div class="row">
    <div class="col-md-9">

    </div>
    <div class="col-md-3">


        <div onclick="goGlobal()" style="position:absolute;right: 20px">
            <img style="width: 90px; height: 90px" src="/images/banner2.png">

        </div>
    </div>
</div>
<div class="limiter">
    <div class="container-login100">
        <div  class="wrap-login100">
            <form class="login100-form validate-form">
					<span  class="login100-form-title p-b-43">
                        <strong>¡Regístrate!</strong> <br><br>

                        <h6>¡Contáctanos y recibe un 20% de descuento en tu primer mensualidad!</h6>

					</span>
                <div class="wrap-input100 validate-input"  data-validate = "Debes poner tu nombre">
                    <input class="input100" type="text" name="nombre" id="inputNombre">
                    <span class="focus-input100"></span>
                    <span class="label-input100">Nombre</span>
                </div>
                <div class="wrap-input100 validate-input" data-validate = "Correo electrónico requerido: ex@abc.xyz">
                    <input class="input100" type="text" name="mail" id="inputMail">
                    <span class="focus-input100"></span>
                    <span class="label-input100">Correo electrónico</span>
                </div>
                <div class="wrap-input100 " >
                    <input class="input100" type="number" name="mail" id="inputTelefono">
                    <span class="focus-input100"></span>
                    <span class="label-input100">Teléfono</span>
                </div>

                <div class="container-login100-form-btn">
                    <input type="button" value="Enviar"  onclick="sendMsj()" class="login100-form-btn">



                </div>



                <div class="login100-form-social flex-c-m">
                    <div class="col-sm">
                        <div style="color:#FFF" class="text-center p-4 mt-3">
                            <div  class="avatar-md mx-auto mb-4">
                                <span class="avatar-title rounded-circle bg-soft-primary">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-tablet icon-dual-primary"><rect x="4" y="2" width="16" height="20" rx="2" ry="2"></rect><line x1="12" y1="18" x2="12.01" y2="18"></line></svg>
                                </span>
                            </div>
                            <h5 class="font-18">COWORKING</h5>
                            <p style="color: #CCC"  class="mb-0">Escritorios compartidos</p>
                        </div>
                    </div>

                    <div class="col-sm">
                        <div style="color:#FFF" class="text-center p-4 mt-3">
                            <div  class="avatar-md mx-auto mb-4">
                                <span class="avatar-title rounded-circle bg-soft-primary">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-grid icon-dual-primary"><rect x="3" y="3" width="7" height="7"></rect><rect x="14" y="3" width="7" height="7"></rect><rect x="14" y="14" width="7" height="7"></rect><rect x="3" y="14" width="7" height="7"></rect></svg>
                                </span>
                            </div>
                            <h5 class="font-18">ESPACIO DE OFICINA</h5>
                            <p style="color: #CCC" class="text3">Espacios físicos para su negocio</p>
                        </div>
                    </div>
                </div>
                <div class="login100-form-social flex-c-m">
                    <div class="col-sm">
                        <div style="color:#FFF" class="text-center p-4 mt-3">
                            <div  class="avatar-md mx-auto mb-4">
                                <span class="avatar-title rounded-circle bg-soft-primary">
                                   <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-layers icon-dual-primary"><polygon points="12 2 2 7 12 12 22 7 12 2"></polygon><polyline points="2 17 12 22 22 17"></polyline><polyline points="2 12 12 17 22 12"></polyline></svg>
                                </span>
                            </div>
                            <h5 class="font-18">OFICINA VIRTUAL</h5>
                            <p style="color: #CCC"  class="mb-0">Concéntrece en el desarrollo de sus proyecto, mientras nosotros nos encargamos de la parte adminsitrativa.</p>
                        </div>
                    </div>

                    <div class="col-sm">
                        <div style="color:#FFF" class="text-center p-4 mt-3">
                            <div  class="avatar-md mx-auto mb-4">
                                <span class="avatar-title rounded-circle bg-soft-primary">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-grid icon-dual-primary"><rect x="3" y="3" width="7" height="7"></rect><rect x="14" y="3" width="7" height="7"></rect><rect x="14" y="14" width="7" height="7"></rect><rect x="3" y="14" width="7" height="7"></rect></svg>
                                </span>
                            </div>
                            <h5 class="font-18">SALA DE JUNTAS</h5>
                            <p style="color: #CCC"  class="mb-0">Realice presentaciones y cierre sus proyectos en su sala de juntas</p>
                        </div>
                    </div>
                </div>
            </form>

            <div class="login100-more" style="background-image: url('/images/fondo2.jpg');">
            </div>
        </div>
    </div>
</div>





<!--===============================================================================================-->
<script src="v18/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="v18/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script src="v18/vendor/bootstrap/js/popper.js"></script>
<script src="v18/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="v18/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="v18/vendor/daterangepicker/moment.min.js"></script>
<script src="v18/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
<script src="v18/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script src="v18/js/main.js"></script>

<script>
    function goGlobal()
    {
        window.open('http://global3corp.com/index.html', '_blank');
    }
</script>
<script src="<?php echo e(asset('alert/bootbox.min.js')); ?>" ></script>
<script src="<?php echo e(asset('alert/bootbox.locales.js')); ?>" ></script>
<script>
    function sendMsj()
    {
        //bootbox.alert("Enviando mensaje...");


        if(document.getElementById('inputMail').value != "")
        {
            if( document.getElementById('inputNombre').value != "")
            {
                //alert("Se envia mensaje y se registra en base de datos");
               bootbox.dialog({ message: '<div class="text-center"><i class="fa fa-spin fa-spinner"></i> Enviando mensaje...</div>' })
                var url = '{{route('sendMail')}}';
              var token = '<?php echo e(csrf_token()); ?>';
                $.ajax({
                    method: 'POST',
                    url: url,
                    data: {
                        mail: document.getElementById('inputMail').value,
                        nombre: document.getElementById('inputNombre').value,
                        telefono: document.getElementById('inputTelefono').value,
                        _token: token
                    }
                })
                    .done(function(msg){
                        console.log(msg);
                        bootbox.alert("¡Mensaje enviado! pronto estaremos en contacto...");
                            document.getElementById('inputMail').value = ""
                             document.getElementById('inputNombre').value = ""
                            document.getElementById('inputTelefono').value = ""
                        bootbox.hideAll();
                    });
            }else
            {
                bootbox.alert("Nos gustaría saber como te llamas, ¡escribe tu nombre!");
            }

        }else
        {
            bootbox.alert("Nos gustaría poder contactarte, por lo menos deja un correo electrónico válido");
        }
    }

</script>

</body>
</html>