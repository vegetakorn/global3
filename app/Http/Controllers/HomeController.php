<?php

namespace App\Http\Controllers;

use App\Mail\MailActividad;
use Illuminate\Http\Request;
use Mail;
use Illuminate\Mail\Mailable;
use Swift_Mailer;
use \Swift_SmtpTransport as SmtpTransport;
use DB;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    public function mail(Request $request)
    {

        // Setup a new SmtpTransport instance for Gmail
        $transport = new SmtpTransport();
        $transport->setHost('mail.hdam.mx');
        $transport->setPort(587);
        $transport->setEncryption('tls');
        $transport->setUsername('info@hdam.mx');
        $transport->setPassword('cj]ay?IIwyA2');


        // Assign a new SmtpTransport to SwiftMailer
        $driver = new Swift_Mailer($transport);

        // Assign it to the Laravel Mailer
        Mail::setSwiftMailer($driver);

        $data['mail'] = $request['mail'];
        $data['nombre'] = $request['nombre'];
        $data['telefono'] = $request['telefono'];

        // Send your message
        Mail::to('milton.rodriguez@piensasoftware.com')->send(new MailActividad($data));
        return response()->json(['message' => "OK"] );

    }
}
